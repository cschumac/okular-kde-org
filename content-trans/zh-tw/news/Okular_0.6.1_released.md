---
date: 2008-02-05
title: Okular 0.6.1 已發布
---
The first maintenance release of the KDE 4.0 series includes Okular 0.6.1. It includes quite a few bug fixes, including better not redownloading files when saving, usability improvements, etc. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
