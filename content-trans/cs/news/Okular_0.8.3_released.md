---
date: 2009-05-06
title: Vydán Okular 0.8.3
---
The third maintenance release of the KDE 4.2 series includes Okular 0.8.3. It does not provide much news for Okular, the only relevant change is about thread more safety when generating page images of XPS document. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
