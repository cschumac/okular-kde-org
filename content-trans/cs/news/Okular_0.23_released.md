---
date: 2015-08-19
title: Vydán Okular 0.23
---
The 0.23 version of Okular has been released together with KDE Applications 15.08 release. This release introduces support for the Fade transition in presentation mode as well as some fixes regarding annotations and video playback. Okular 0.23 is a recommended update for everyone using Okular.
