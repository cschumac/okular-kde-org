---
menu:
  main:
    name: Zbuduj to
    parent: about
    weight: 2
title: Budowanie Okulara ze źródeł na Linuksie
---
<span style="background-color:#e8f4fa">Jeśli szukasz uprzednio zbudowanych pakietów, to odwiedź [stronę pobierania](/download/). Stan pakowania możesz sprawdzić [tutaj](https://repology.org/project/okular/versions)</span>

Jeśli chcesz zbudować Okulara, to musisz mieć ustawione środowisko do budowania, które w ogólności powinno ci być zapewnione przez twoją dystrybucję. Jeśli chcesz budować wersję rozwojową Okulara, to przejdź do działu budowania z kodu źródłowego na Wiki Społeczności KDE.

Możesz pobrać i skompilować Okulara w następujący sposób:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Jeśli wgrywasz Okulara w innym katalogu niż systemowym, to prawdopodobnie musisz uruchomić `source build/prefix.sh; okular` tak aby pobrane został poprawne wystąpienie Okulara i bibliotek.

## Opcjonalne pakiety

Istnieje kilka niewymaganych pakietów, które możesz wgrać, aby zyskać więcej możliwości w Okularze. Niektóre z nich mogą być już w twojej dystrybucji, a innych może tam nie być. Jeśli chcesz uniknąć kłopotów, to trzymaj się pakietów swojej dystrybucji.

* Poppler (silnik PDF): Do zbudowania silnika PDF potrzebujesz [biblioteki Poppler](http://poppler.freedesktop.org), dla której wymagana wersja to co najmniej 0.24
* Libspectre: Aby zbudować i używać silnika PostScipt (PS), potrzebujesz  >= 0.2. Jeśli twoje distro go nie zawiera lub dostarczana wersja nie jest wystarczająca, to możesz ją pobrać [stąd](http://libspectre.freedesktop.org)
* DjVuLibre: Aby zbudować silnik DjVu, potrzebujesz DjVuLibre >= 3.5.17. Tak jak w przypadku Libspectre, możesz pobrać ją ze swojego distro lub [tutaj](http://djvulibre.djvuzone.org).
* libTIFF: Jest to wymagane do obsługi TIFF/fax. Obecnie nie minimalnej wymaganej wersji, więc wszelkie stosunkowo aktualne wydania biblioteki dostępne dla twojej dystrybucji powinny działać. W przypadku kłopotów zachęcamy do kontaktu z twórcami Okulara.
* libCHM: Jest to potrzebne do zbudowania silnika CHM. Tak jak z libTIFF, nie ma wymagania na najstarszą wersję
* Libepub: Jeśli potrzebujesz obsługi EPub, to możesz wgrać tę bibliotekę ze swojego distro lub z [sourceforge](http://sourceforge.net/projects/ebook-tools).
