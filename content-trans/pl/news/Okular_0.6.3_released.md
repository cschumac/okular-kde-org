---
date: 2008-04-02
title: Okular 0.6.3 wydany
---
Trzecie wydanie podtrzymujące linię KDE 4.0 obejmuje Okular 0.6.3. Zawiera kilka poprawek, np. lepszy sposób na pobranie pozycji tekstu w dokumencie PDF, kilka poprawek dla systemu adnotacji oraz spis treści. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
