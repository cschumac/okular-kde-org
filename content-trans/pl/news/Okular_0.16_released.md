---
date: 2013-02-06
title: Okular 0.16 wydany
---
Wersja 0.16 Okulara została wydana wraz z wydaniem Aplikacji KDE 4.10. Wydanie to wprowadza nowe funkcje, takie jak możliwość przybliżania do wyższych poziomów w dokumentach PDF, przeglądarkę opartą na Active dla urządzeń typu tablet, więcej wsparcia dla filmów PDF, widok podążający za wyborem i ulepszenia do edytowania przypisów. Okular 0.16 jest zalecanym uaktualnieniem dla każdego użytkownika Okulara.
