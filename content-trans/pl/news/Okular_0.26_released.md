---
date: 2016-08-18
title: Okular 0.26 wydany
---
Wersja 0.26 Okulara została wydana wraz z wydaniem Aplikacji do KDE 16.08. Wydanie to wprowadza pomniejsze poprawki błędów i nowe funkcje, pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Zaleca się uaktualnienie do Okulara 0.26 wszystkim użytkownikom.
