---
date: 2007-01-31
title: Okular dołączył do projektu The Season of Usability
---
Zespół Okular dumnie ogłasza, że Okular jest jedną z aplikacji wybranych do udziału w projekcie <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, prowadzonym przez ekspertów użyteczności z <a href="http://www.openusability.org">OpenUsability</a>. Z tego miejsca chcemy przywitać w zespole Sharada Baliyana oraz podziękować Florianowi Graessle i Pino Toscano za kontynuowanie ich pracy nad ulepszaniem Okular.
