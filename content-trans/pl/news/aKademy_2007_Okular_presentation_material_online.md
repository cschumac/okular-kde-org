---
date: 2007-07-10
title: aKademy 2007 - prezentacja Okular (materiały online)
---
Wystąpienie nt. Okulara wygłoszone przez Pino Toscano na <a href="http://akademy2007.kde.org">aKademy 2007</a> od teraz jest w internecie. Dostępne są zarówno  <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">strony z prezentacji</a> oraz<a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">filmy</a>.
