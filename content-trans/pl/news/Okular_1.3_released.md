---
date: 2017-12-14
title: Okular 1.3 wydany
---
Wersja 1.3 Okulara została wydana wraz z wydaniem Aplikacji do KDE 17.12. To wydanie zmienia sposób zapisywania przypisów i formularzy, obsługuje cząstkowe uaktualnienia do wyświetlania dla plików, które długo się wczytują, umożliwia klikanie na odnośniki w trybie zaznaczania tekstu, dodaje możliwość współdzielenia w menu pliku, dodaje obsługę Markdown oraz naprawia niektóre z błędów w obsłudze HiDPI. Pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.3 wszystkim użytkownikom.
