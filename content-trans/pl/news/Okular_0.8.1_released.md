---
date: 2009-03-04
title: Okular 0.8.1 wydany
---
Pierwsze wydanie podtrzymujące linię KDE 4.2 obejmuje Okular 0.8.1. Zawiera ono ważne poprawki w modułach CHM i DjVu oraz drobne ulepszenia interfejsu użytkownika. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
