---
date: 2021-04-22
title: Okular 21.04 wydany
---
Wydano wersję 21.04 Okulara. To wydanie wprowadza cyfrowe podpisywanie plików PDF i wiele innych poprawek w różnych miejscach. Pełny dziennik zmian możesz obejrzeć na <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. Zaleca się uaktualnienie do Okulara 21.04 wszystkim użytkownikom.
