---
date: 2012-10-10
title: Fora Okulara
---
Od teraz mamy podforum wewnątrz forum Społeczności KDE. Możesz je znaleźć pod adresem <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
