---
date: 2020-04-23
title: Okular 1.10 wydany
---
Wydano wersję 1.10 Okulara. To wydanie wprowadza kinetyczne przewijanie, usprawnienia do zarządzania kartami, usprawnienia do interfejsu na urządzeniach przenośny i wiele innych poprawek w różnych miejscach. Pełny dziennik zmian możesz obejrzeć na <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.10 wszystkim użytkownikom.
