---
date: 2008-07-29
title: Okular 0.7 wydany
---
Zespół Okulara ma zaszczyt ogłosić nową wersję Okulara, wydaną jako część <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Wśród nowych możliwości i usprawnień znajdują się (w losowej kolejności):

* Możliwość zapisu dokumentu PDF ze zmianami do pól formularzy.
* Tryb prezentacji: obsługa dla więcej niż jednego ekranu i możliwość wyłączenia przejść
* Poziom powiększenia jest od teraz zapamiętywany osobno dla każdego dokumentu
* Usprawniona obsługa tekstu-na-mowę, której można używać do odczytu całego dokumentu, danej strony lub zaznaczonego tekstu
* Wyszukiwanie tekstu wstecz
* Usprawniona obsługa formularzy
* Wstępny (naprawdę podstawowa i najprawdopodobniej niepełna) obsługa JavaScript w dokumentach PDF
* Obsługa opisu w postaci plików załączanych
* Możliwość usunięcia białego obramowania stron podczas ich przeglądania
* Nowy silnik dla dokumentów EPub
* Silnik tekstu OpenDocument: obsługa zaszyfrowanych dokumentów, usprawnienia do list oraz tabel
* Silnik XPS: różne usprawnienia do wczytywania i wyświetlania, od teraz powinno być bardziej użyteczne
* Silnik PS: różne usprawnienia, gównie ze względu na wymaganie nowszego libspectre
