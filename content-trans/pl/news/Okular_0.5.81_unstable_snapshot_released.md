---
date: 2006-11-02
title: Okular 0.5.81 (wersja niestabilna) wydany
---
Zespół Okular dumnie ogłasza wydanie migawki Okulara dla <a href="http://dot.kde.org/1162475911/">Second KDE 4 Developers Snapshot</a>. Migawka wciąż nie jest całkowicie funkcjonalna, mamy wiele rzeczy do dopracowania, ale jeśli chcecie, możecie ją przetestować i dostarczyć jak najwięcej informacji zwrotnych. Pakiet można znaleźć na <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Odwiedź także stronę <a href="download.php">pobierania</a> i upewnij się, że masz wszystkie niezbędne biblioteki.
