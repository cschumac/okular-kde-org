---
intro: Okular obsługuje wiele formatów dokumentów i przypadków używania. Ta strona
  zawsze odnosi się do stabilnych wydań Okulara, obecnie Okulara 20.12
layout: formats
menu:
  main:
    name: Format dokumentu
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Stan programów obsługujących formaty dokumentów
---
