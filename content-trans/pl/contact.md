---
menu:
  main:
    parent: about
    weight: 4
title: Kontakt
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

Z twórcami Okular możesz skontaktować się na wiele sposobów:

* Lista rozmów: Do zarządzania rozwojem okulara, używamy [listy rozmów okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) na kde.org. Możesz jej użyć do rozmowy o rozwoju głównej aplikacji oraz wyrażenia opinii o istniejących lub nowych silnikach.

* IRC: Do ogólnych rozmów używamy IRC [#okular](irc://irc.kde.org/#okular) oraz [#kde-devel](irc://irc.kde.org/#kde-devel) w [sieci Freenode](http://www.freenode.net/). Można tam znaleźć niektórych twórców Okulara.

* Matrix: Do uprzednio wspomnianego miejsca rozmów można także dotrzeć poprzez sieć Matrix na [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum: Jeśli wolisz używać forum, to możesz używać [Forum Okulara](http://forum.kde.org/viewforum.php?f=251) wewnątrz większych [For społeczności KDE](http://forum.kde.org/).

* Błędy i życzenia: Błędy i życzenia powinny być zgłaszane do [Systemu obsługi błędów KDE](https://bugs.kde.org/). Jeśli chcesz pomóc, to spis głównych błędów możesz znaleźć [tutaj](https://community.kde.org/Okular).
