---
intro: Okular jest dostępny jako uprzednio zbudowany pakiet na wielu platformach.
  Możesz sprawdzić stan pakietu dla swojej dystrybucji Linuksa po prawej lub przeczytać
  szczegóły nt. innych systemów operacyjnych.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular jest już dostępny na większości dystrybucji Linuksowych. Możesz go
    wgrać z [Ośrodka Oprogramowania KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logo Flatpaka
  name: Flatpak
  text: Możesz wgrać najnowszego [Flatpaka Okulara](https://flathub.org/apps/details/org.kde.okular)
    z Flathuba. Rozwojowe Flatpaki lub Okulara budowanego nocą można [wgrać z repozytorium
    KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications ).
- image: /images/ark.svg
  image_alt: Logo Arka
  name: Wydanie źródeł
  text: Okular jest wydawany regularnie jako część Zestawu KDE. Jeśli chcesz go zbudować
    ze źródła, to zajrzyj do [działu budowania](/build-it).
- image: /images/windows.svg
  image_alt: Logo Windowsa
  name: Windows
  text: Spójrz na [inicjatywę KDE na Windowsie](https://community.kde.org/Windows),
    jeśli chcesz wiedzieć jak wgrać oprogramowanie KDE na Windowsie. Stabilne wydanie
    jest dostępne w [Sklepie Microsoft](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Istnieją także [wydania budowane w nocy](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    dla których testy i zgłoszenia o błędach będą mile widziane.
sassFiles:
- /sass/download.scss
title: Pobierz
---
