---
date: 2017-12-14
title: Выпущен Okular 1.3
---
The 1.3 version of Okular has been released together with KDE Applications 17.12 release. This release introduces changes to how saving annotations and form data works, supports partial rendering updates for files that take long time to render, makes text links interactive on text selection mode, adds a Share option in the File menu, adds Markdown support and fixes some issues regarding HiDPI support. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 is a recommended update for everyone using Okular.
