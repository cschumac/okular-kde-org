---
date: 2016-12-15
title: Выпущен Okular 1.0
---
The 1.0 version of Okular has been released together with KDE Applications 16.12 release. This release is now based in KDE Frameworks 5, you can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 is a recommended update for everyone using Okular.
