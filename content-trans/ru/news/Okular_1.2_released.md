---
date: 2017-08-17
title: Выпущен Okular 1.2
---
The 1.2 version of Okular has been released together with KDE Applications 17.08 release. This release introduces minor bugfixes and improvements. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 is a recommended update for everyone using Okular.
