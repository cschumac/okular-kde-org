---
date: 2008-10-03
title: Выпущен Okular 0.7.2
---
Второй выпуск KDE 4.1 включает в себя Okular 0.7.2. В Okular 0.7.2 были исправлены некоторые небольшие ошибки при работе с модулями поддержки TIFF и Comicbook, а также «По ширине страницы» был установлен как основной масштаб. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
