---
date: 2020-04-23
title: Выпущен Okular 1.10
---
The 1.10 version of Okular has been released. This release introduces kinetic scrolling, tab management improvements, improvements to the Mobile UI and various minor fixes and features all over. You can check the full changelog at <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 is a recommended update for everyone using Okular.
