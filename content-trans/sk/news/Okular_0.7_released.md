---
date: 2008-07-29
title: Vydaný Okular 0.7
---
The Okular team is proud to announce a new version of Okular, released as part of <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Some of the new features and improvements include (in random order):

* Possibility to save a PDF document with the changes to the form fields
* Presentation mode: support for more than one screen, and possibility to disable transitions
* The zoom level is stored per-document now
* Enhanced text-to-speech support, that can be used to read the whole document, a specific page or the text selection
* Backward direction for text search
* Improved support for forms
* Preliminary (really basic and most probably incomplete) support for JavaScript in PDF documents
* Support for file attachment annotations
* Possibility to remove the white border of pages when viewing pages
* New backend for EPub documents
* OpenDocument Text backend: support for encrypted documents, list and tables improvements
* XPS backend: various loading and rendering improvements, should be much more usable now
* PS backend: various improvements, mainly due to the newer libspectre required
