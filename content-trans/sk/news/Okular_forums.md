---
date: 2012-10-10
title: Fóra Okular
---
We now have a subforum inside the KDE Community Forums. You can find it at <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
