---
date: 2018-08-16
title: O Okular 1.5 foi lançado
---
A versão 1.5 do Okular foi lançada junto com o KDE Applications versão 18.08. Esta versão introduz melhorias nos formulários entre várias outras correções e melhorias. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. O Okular 1.5 é uma atualização recomendada a todos os usuários do Okular.
