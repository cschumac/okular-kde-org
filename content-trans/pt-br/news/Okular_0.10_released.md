---
date: 2010-02-09
title: O Okular 0.10 foi lançado
---
A versão 0.10 do Okular foi lançada junto com o KDE SC 4.4. Além de algumas melhorias gerais na estabilidade, essa versão possui um suporte novo/melhorado tanto para a pesquisa normal como a inversa, assim como a associação de linhas de código em LaTeX aos locais correspondentes nos arquivos DVI e PDF.
