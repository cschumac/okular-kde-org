---
date: 2009-12-01
title: O Okular 0.9.4 foi lançado
---
A quarta versão de manutenção da série KDE 4.3 inclui o Okular 0.9.4. Ela incorpora algumas correções de falhas e algumas pequenas correções de erros na interface. Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
