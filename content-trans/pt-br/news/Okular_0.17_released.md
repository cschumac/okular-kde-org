---
date: 2013-08-16
title: O Okular 0.17 foi lançado
---
A versão 0.17 do Okular foi lançada junto com a versão 4.11 do KDE Applications. Esta versão introduz novas funcionalidades, como suporte a desfazer/refazer para formulários e anotações, assim como ferramentas de revisão configuráveis. É uma atualização recomendada a todos os usuários do Okular.
