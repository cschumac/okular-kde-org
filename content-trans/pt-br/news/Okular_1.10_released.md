---
date: 2020-04-23
title: O Okular 1.10 foi lançado
---
A versão 1.10 do Okular foi lançada. Esta versão introduz o suporte à rolagem cinética, melhorias no gerenciamento de abas, melhorias na interface móvel e várias correções menores e melhorias por todo lugar. Você pode verificar o registro de alterações completo em <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. O Okular 1.10 é uma atualização recomendada a todos os usuários do Okular.
