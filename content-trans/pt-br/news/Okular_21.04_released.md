---
date: 2021-04-22
title: O Okular 21.04 foi lançado
---
A versão 21.04 do Okular foi lançada. Esta versão introduz a Assinatura Digital de ficheiros PDF e diversas correcções e funcionalidades pequenas em todo o lado. Poderá verificar o registo de alterações completo em <a href='https://kde.org/announcements/releases/21.04.0/#okular'>https://kde.org/announcements/releases/21.04.0/#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
