---
date: 2008-07-29
title: O Okular 0.7 foi lançado
---
A equipa do Okular orgulha-se em anunciar uma nova versão do Okular, lançada como parte do <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Algumas das novas funcionalidades e melhorias incluem (por ordem aleatória):

* Possibilidade de gravar um documento PDF com as alterações dos campos do formulário
* Modo de apresentação: suporte para mais de um ecrã e a possibilidade de desactivar as transições
* O nível de ampliação agora é guardado por documento
* Suporte melhorado de texto-para-fala, o qual poderá ser usado para ler o documento inteiro, uma página em particular ou a selecção de texto
* Pesquisa de texto para trás
* Suporte melhorado para os formulários
* Suporte preliminar (realmente básico e muito provavelmente incompleto) para o JavaScript nos documentos PDF
* Suporte para anotações com ficheiros em anexo
* Possibilidade de remover o contorno branco das páginas ao ver as mesmas
* Nova infra-estrutura para os documentos EPub
* Infra-estrutura de Texto OpenDocument: suporte para documentos encriptados, melhorias nas listas e tabelas
* Infra-estrutura de XPS: diversas melhorias no carregamento e visualização, o que a deverá tornar mais útil agora
* Infra-estrutura de PS: diversas melhorias, principalmente devido à nova versão obrigatória da 'libspectre'
