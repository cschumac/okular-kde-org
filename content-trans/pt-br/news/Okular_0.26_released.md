---
date: 2016-08-18
title: O Okular 0.26 foi lançado
---
A versão 0.26 do Okular foi lançada junto com o KDE Applications versão 16.08. Esta versão introduz pequenas alterações, você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. O Okular 0.26 é uma atualização recomendada a todos os usuários do Okular.
