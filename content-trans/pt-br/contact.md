---
menu:
  main:
    parent: about
    weight: 4
title: Contato
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, a mascote do KDE" style="height: 200px;"/>

Pode contactar a equipa do Okular de várias formas:

* Lista de correio: Para coordenar o desenvolvimento do Okular, usamos a [lista de correio okular-devel(https://mail.kde.org/mailman/listinfo/okular-devel). Podê-la-á usar para falar acerca do desenvolvimento da aplicação de base, mas também poderá dar sugestões sobre infra-estruturas novas ou existentes.

* IRC: Para conversas gerais, usamos os canais [#okular](irc://irc.kde.org/#okular) e [#kde-devel](irc://irc.kde.org/#kde-devel) do IRC na [rede Freenode](http://www.freenode.net/). Alguns dos programadores do Okular poderão ser encontrados aí.

* Matrix: A conversa referida acima também poderá ser acedida através da rede Matrix em [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Fórum: Se preferir usar um fórum, poderá usar o [fórum do Okular](http://forum.kde.org/viewforum.php?f=251) dentro dos [Fóruns da Comunidade do KDE](http://forum.kde.org/).

* Erros e Sugestões: Os erros e sugestões deverão ser comunicados ao sistema de registo de erros do KDE no [sistema de registo de erros do KDE](http://bugs.kde.org). Se quiser ajudar, poderá encontrar uma lista dos erros de topo [aqui](https://community.kde.org/Okular).
