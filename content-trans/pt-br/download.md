---
intro: O Okular está disponível como um pacote pré-compilado numa grande gama de plataformas.
  Poderá verificar o estado dos pacotes da sua distribuição de Linux à direita ou
  continue a ler mais informações sobre outros sistemas operativos
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: O Okular já está disponível na maioria das distribuições de Linux. Podê-lo-á
    instalar no [Centro de Aplicações do KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logótipo do Flatpak
  name: Flatpak
  text: Poderá instalar a última versão do [Flatpak do Okular](https://flathub.org/apps/details/org.kde.okular)
    do Flathub. Os Flatpak's experimentais com versões diárias do Okular podem ser
    [instalados a partir do repositório de Flatpak do KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logótipo do Ark
  name: Código das Versões
  text: O Okular é lançado com regularidade como parte do KDE Gear. Se quiser compilar
    a partir do código, poderá consultar a [secção Compilar](/build-it).
- image: /images/windows.svg
  image_alt: Logótipo do Windows
  name: Windows
  text: Para o Windows, dê uma vista de olhos na página Web [Iniciativa do KDE no
    Windows](https://community.kde.org/) para saber mais informações como instalar
    o KDE no Windows. A versão estável está disponível na [Loja da Microsoft](https://www.microsoft.com/store/apps/9N41MSQ1WNM8).
    Também existem [versões experimentais nocturnas](https://binary-factory.kde.org/job/Okular_Nightly_win64/).
    Por favor teste e envie modificações.
sassFiles:
- /sass/download.scss
title: Download
---
