---
menu:
  main:
    name: Compilalo
    parent: about
    weight: 2
title: Compilazione di Okular da sorgente in Linux
---
<span style="background-color:#e8f4fa">Se stai cercando pacchetti precompilati, visita la [pagina degli scaricamenti](/download/). Puoi verificare lo stato dei pacchetti [qui](https://repology.org/project/okular/versions)</span>

Se desideri compilare Okular, devi avere impostato un ambiente per la compilazione che, in linea generale, deve essere fornito dalla tua distribuzione. Nel caso tu voglia compilare le versioni di sviluppo di Okular, visita la pagina <a href='%1'>Build from source</a> nel wiki Community di KDE.

Puoi scaricare e compilare Okular in questo modo:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Se installi Okular in un percorso differente dalla cartella di installazione, forse avrai necessità di eseguire `source build/prefix.sh; okular` in modo che siano usate l'istanza e le librerie corrette di Okular.

## Pacchetti opzionali

Esistono diversi pacchetti opzionali che puoi installare per ottenere più funzionalità in Okular. Alcuni potrebbero essere inclusi per la tua distribuzione, altri no. Se vuoi evitare di avere problemi, attieniti ai pacchetti supportati dalla tua distribuzione

* Poppler (motore PDF): per compilare il motore PDF, hai bisogno della [libreria Poppler](http://poppler.freedesktop.org), per la quale è richiesta la versione minima 0.24
* Libspectre: per compilare e usare questo motore PostScript (PS), hai bisogno di libspectre >= 0.2. Se la tua distribuzione non lo contiene, o la versione inclusa è precedente, puoi scaricarlo da [qui](http://libspectre.freedesktop.org)
* DjVuLibre: per compilare il motore DjVu, hai bisogno di DjVuLibre >= 3.5.17. Allo stesso modo che Libspectre, puoi ottenerlo dalla tua distribuzione o da [qui](http://djvulibre.djvuzone.org).
* libTIFF: è richiesto per il supporto TIFF/fax. Attualmente non esiste una versione minima richiesta, dunque qualsiasi versione recente della libreria disponibile per la tua distribuzione dovrebbe funzionare. In caso di problemi, non esitare a contattare gli sviluppatori di Okular.
* libCHM: è necessario per compilare il motore CHM. Allo stesso modo di libTIFF, non è richiesta una versione minima
* Libepub: se necessiti di supporto EPub, puoi installare questa libreria dalla tua distribuzione o da [sourceforge](http://sourceforge.net/projects/ebook-tools).
