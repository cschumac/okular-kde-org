---
menu:
  main:
    parent: about
    weight: 4
title: Contatta
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, la mascotte di KDE" style="height: 200px;"/>

Puoi contattare la squadra di Okular in vari modi:

* Mailing list: per coordinare lo sviluppo di Okular utilizziamo la [mailing list okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) in kde.org. Puoi usarla per parlare dello sviluppo dell'applicazione principale, ed è anche apprezzato l'invio delle impressioni sui motori esistenti o nuovi.

* IRC: per la chat generale utilizziamo IRC [#okular](irc://irc.kde.org/#okular) e [#kde-devel](irc://irc.kde.org/#kde-devel) sulla [rete Freenode](http://www.freenode.net/). Qui puoi trovare diversi sviluppatori di Okular.

* Matrix: la chat summenzionata è accessibile anche sulla rete Matrix tramite [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum: se preferisci i forum, puoi usare il [forum di Okular](http://forum.kde.org/viewforum.php?f=251), che si trova all'interno della più vasta [forum della comunità KDE](http://forum.kde.org/).

* Errori e suggerimenti: dovrebbero essere segnalati nel [sistema di tracciamento dei bug di KDE](https://bugs.kde.org). Se vuoi collaborare, è presente un elenco degli errori più importanti [qui](https://community.kde.org/Okular).
