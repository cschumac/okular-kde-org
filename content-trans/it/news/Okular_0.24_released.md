---
date: 2015-12-16
title: Rilasciato Okular 0.24
---
La versione 0.24 di Okular è stato rilasciata insieme con le applicazioni di KDE 15.12. Questo rilascio introduce correzioni e funzionalità minori, puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
