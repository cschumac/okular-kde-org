---
date: 2007-04-04
title: Okular in kdegraphics
---
La squadra di Okular è orgogliosa di annunciare che Okular fa ora parte del modulo kdegraphics. La prossima versione di Okular sarà distribuita con KDE 4.0.
