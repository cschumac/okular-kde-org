---
date: 2021-04-22
title: Rilasciato Okular 21.04
---
È stata rilasciata la versione 21.04 di Okular. Questo rilascio introduce la firma digitale dei file PDF, oltre a varie correzioni minori e aggiunte di funzionalità dappertutto. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
