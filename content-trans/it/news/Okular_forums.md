---
date: 2012-10-10
title: Forum di Okular
---
Ora abbiamo un forum secondario all'interno dei forum della comunità KDE. Puoi trovarlo in <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
