---
date: 2020-12-10
title: Publication de la version 20.12 d'Okular
---
La version 20.12 d'Okular a été publiée. Cette version intègre de nombreuses corrections et fonctionnalités sur l'ensemble du logiciel. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. La version 20.12 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
