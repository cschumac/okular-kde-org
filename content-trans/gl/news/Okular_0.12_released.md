---
date: 2011-01-26
title: Publicouse Okular 0.12
---
A versión 0.12 de Okular publicouse xunto coa versión 4.6 das aplicacións de KDE. Esta versión introduce pequenas correccións e funcionalidades e recoméndase para calquera que use Okular.
