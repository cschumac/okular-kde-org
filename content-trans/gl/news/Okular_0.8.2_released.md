---
date: 2009-04-02
title: Publicouse Okular 0.8.2
---
A segunda versión de mantemento da serie 4.2 de KDE inclúe Okular 0.8.2. Inclúe unha mellor compatibilidade (con sorte funcional) con DVI e a busca inversa de pdfsync, e correccións e pequenas melloras no modo de presentación. Pode ler os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
