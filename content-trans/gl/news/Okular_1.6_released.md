---
date: 2018-12-13
title: Publicouse Okular 1.6
---
A versión 1.6 de Okular publicouse coa versión 18.12 das aplicacións de KDE. Esta versión introduce a nova ferramenta de anotación de escritura e moitos outras solucións de erros e pequenas funcionalidades. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0#okular</a>. Okular 1.6 é unha actualización recomendada para calquera que use Okular.
