---
date: 2010-02-09
title: Publicouse Okular 0.10
---
A versión 0.10 de Okular publicouse xunto coa versión 4.4 da colección de software de KDE. Ademais de melloras xerais en estabilidade, esta versión inclúe funcionalidade nova e mellorada de busca inversa e busca normal ligando as liñas de código fonte de LaTeX cos lugares correspondentes dos ficheiros DVI ou PDF.
