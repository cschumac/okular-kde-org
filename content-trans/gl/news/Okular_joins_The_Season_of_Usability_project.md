---
date: 2007-01-31
title: Okular únese ao proxecto de Tempada de Facilidade de Uso (Season of Usability)
---
The Okular team is proud to announce Okular has been one of the applications selected to participate in the <a href="http://www.openusability.org/season/0607/">Season of Usability</a> project, run by usability experts at <a href="http://www.openusability.org">OpenUsability</a>. From here we want to welcome Sharad Baliyan to the team and thank Florian Graessle and Pino Toscano for their continued work on improving Okular.
