---
date: 2014-04-16
title: Publicouse Okular 0.19
---
A versión 0.19 de Okular publicouse xunto coa versión 4.13 das aplicacións de KDE. Esta versión introduce novas funcionalidades como separadores na interface, o uso de pantallas DPI para que o tamaño das páxinas se corresponda co tamaño real en papel, melloras coa infraestrutura de desfacer e refacer, e outras funcionalidades e refinamentos. Okular 0.19 é unha actualización recomendada para calquera que use Okular.
