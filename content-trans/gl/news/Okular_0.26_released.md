---
date: 2016-08-18
title: Publicouse Okular 0.26
---
A versión 0.26 de Okular publicouse coa versión 16.08 das aplicacións de KDE. Esta versión introduce moitos cambios pequenos, pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 é unha actualización recomendada para calquera que use Okular.
