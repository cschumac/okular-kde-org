---
date: 2015-08-19
title: Okular 0.23 vrijgegeven
---
De 0.23 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 15.08. Deze vrijgave introduceert ondersteuning voor overgangen met vervagings in modus presentatie evenals reparaties met betrekking tot annotaties en afspelen van video. Okular 0.23 wordt aanbevolen aan iedereen die Okular gebruikt.
