---
date: 2010-02-09
title: Okular 0.10 vrijgegeven
---
De 0.10 versie van Okular is vrijgegeven samen met KDE SC 4.4. Naast algemene verbeteringen in stabiliteit kreeg deze uitgave nieuw/verbeterde ondersteuning voor zowel omgekeerd en voorwaarts zoeken waarmee broncoderegels in latex gekoppeld worden aan de overeenkomstige locaties in dvi- en pdf-bestanden.
