---
date: 2016-12-15
title: Okular 1.0 vrijgegeven
---
De 1.0 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 16.12. Deze uitgave is nu gebaseerd op KDE Frameworks 5, u kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Okular 1.0 wordt aanbevolen aan iedereen die Okular gebruikt.
