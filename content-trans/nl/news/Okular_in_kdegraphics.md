---
date: 2007-04-04
title: Okular in kdegraphics
---
Het team van Okular is er trots op dat Okular nu onderdeel is van de module kdegraphics. De volgende versie van Okular zal worden meegeleverd met KDE 4.0.
