---
date: 2020-08-13
title: Okular 1.11 released
---
De 1.11 versie van Okular is vrijgegeven. Deze vrijgave introduceert een nieuw gebruikersinterface voor annotaties en overal verschillende kleine reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 wordt aanbevolen voor iedereen die Okular gebruikt.
