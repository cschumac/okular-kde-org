---
date: 2021-04-22
title: Okular 21.04 vrijgegeven
---
De versie 21.04 van Okular is vrijgegeven. Deze vrijgave introduceert digitaal ondertekenen van PDF-bestanden en her en der verschillende kleine reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. Okular 21.04 wordt aanbevolen voor iedereen die Okular gebruikt.
