---
date: 2006-11-02
title: Okular 0.5.81 niet stabiele versie vrijgegeven
---
Het team van Okular is er trots op om de vrijgave van een versie van Okular aan te kondigen die compileert tegen de <a href="http://dot.kde.org/1162475911/">Second KDE 4 Developers Snapshot</a>. Deze versie is nog niet volledig functioneel omdat er nog veel dingen opgepoetst en beëindigd moeten worden, maar u bent vrij om het te testen en zo veel mogelijk terugkoppeling te geven. U kunt het snapshot pakket vinden op <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Kijk ook eens op de <a href="download.php">download</a>-pagina om er zeker van te zijn dat u alle noodzakelijke bibliotheken hebt.
