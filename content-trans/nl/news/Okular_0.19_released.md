---
date: 2014-04-16
title: Okular 0.19 vrijgegeven
---
De 0.19 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.13. Deze vrijgave introduceert nieuwe functies zoals ondersteuning voor tabbladen in het interface, gebruik van de DPI van het scherm, zodat de afmetingen van de pagina overeenkomen met de afmetingen van het papier, verbeteringen aan het ongedaan maken/opnieuw doen en andere functies/verfijningen. Okular 0.19 wordt aanbevolen aan iedereen die Okular gebruikt.
