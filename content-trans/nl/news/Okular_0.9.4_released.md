---
date: 2009-12-01
title: Okular 0.9.4 vrijgegeven
---
De vierde onderhoudsuitgave van de KDE 4.3 serie bevat Okular 0.9.4. Het bevat reparaties van enige crashes en een paar kleine bugreparaties in het interface. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
