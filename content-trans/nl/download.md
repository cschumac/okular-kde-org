---
intro: Okular is beschikbaar als een voorgecompileerd pakket op een brede reeks platforms.
  U kunt de pakketstatus voor uw Linux distributie rechts controleren of blijven lezen
  voor informatie op andere besturingssystemen
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular is al beschikbaar op de meeste Linux distributies. U kunt het installeren
    uit het [KDE Software centrum](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Flatpak-logo
  name: Flatpak
  text: U kunt de laatste [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    uit Flathub installeren. Experimentele Flatpaks met nachtelijke builds van Okular
    kunnen [geïnstalleerd worden uit de KDE Flatpak opslagruimte](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Ark-logo
  name: Uitgavebronnen
  text: Okular wordt regelmatig uitgegeven als onderdeel van KDE Gear. Als u vanaf
    de broncode wilt bouwen dan kunt u de [sectie bouw het](/build-it) bekijken.
- image: /images/windows.svg
  image_alt: Windows-logo
  name: Windows
  text: Kijk op de webpagina [KDE on Windows initiative](https://community.kde.org/Windows)
    voor informatie over hoe KDE Software onder Windows te installeren. De stabiele
    uitgave is beschikbaar in de [Microsoft Store](https://www.microsoft.com/store/apps/9N41MSQ1WNM8).
    Er zijn ook [experimentele nachtelijke builds](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    waarvoor testen en rapporten voor bugs welkom zijn.
sassFiles:
- /sass/download.scss
title: Downloaden
---
