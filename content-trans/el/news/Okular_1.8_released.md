---
date: 2019-08-15
title: Κυκλοφορία της έκδοσης 1.8 του Okular
---
Η έκδοση 1.8 του Okular κυκλοφόρησε μαζί με την έκδοση 19.08 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει το χαρακτηριστικό της ρύθμισης των καταλήξεων του σχολιασμού γραμμών ανάμεσα σε άλλες διορθώσεις και χαρακτηριστικά. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.8.
