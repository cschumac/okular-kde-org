---
date: 2008-09-03
title: Okular 0.7.1 released
---
The first maintenance release of the KDE 4.1 series includes Okular 0.7.1. It includes some crash fixes among other minor fixes. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
