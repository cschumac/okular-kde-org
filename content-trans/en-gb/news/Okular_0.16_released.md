---
date: 2013-02-06
title: Okular 0.16 released
---
The 0.16 version of Okular has been released together with KDE Applications 4.10 release. This release introduces new features like the ability to zoom to higher levels in PDF documents, an Active based viewer for your tablet devices, more support for PDF movies, viewport follows selection, and annotation editing improvements. Okular 0.16 is a recommended update for everyone using Okular.
