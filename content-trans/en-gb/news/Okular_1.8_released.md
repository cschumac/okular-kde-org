---
date: 2019-08-15
title: Okular 1.8 released
---
The 1.8 version of Okular has been released together with KDE Applications 19.08 release. This release introduces the feature of setting Line Annotation endings among various other fixes and small features. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Okular 1.8 is a recommended update for everyone using Okular.
