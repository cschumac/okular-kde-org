---
date: 2017-04-20
title: Okular 1.1 utgiven
---
Version 1.1 av Okular har givits ut tillsammans med utgåva 17.04 av KDE:s program. Utgåvan introducerar funktionalitet för att ändra storlek på kommentarer, stöd för automatisk beräkning av formulärinnehåll, förbättringar för pekskärmar med mera. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 är en rekommenderad uppdatering för alla som använder Okular.
