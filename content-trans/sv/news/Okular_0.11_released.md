---
date: 2010-08-10
title: Okular 0.11 utgiven
---
Version 0.11 av Okular har givits ut tillsammans med utgåva 4.5 av KDE:s program. Utgåvan introducerar mindre rättelser och funktioner och utgör en rekommenderad uppdatering för alla som använder Okular.
