---
date: 2014-04-16
title: Okular 0.19 utgiven
---
Version 0.19 av Okular har givits ut tillsammans med utgåva 4.13 av KDE:s program. Utgåvan introducerar nya funktioner som stöd för flikar i gränssnittet, användning av skärmens bildpunkter per tum så att sidstorleken motsvarar den verkliga pappersstorleken, förbättringar i ramverket för ångra och gör om, samt andra funktioner och förfiningar. Okular 0.19 är en rekommenderad uppdatering för alla som använder Okular.
