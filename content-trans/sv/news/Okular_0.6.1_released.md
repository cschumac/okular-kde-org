---
date: 2008-02-05
title: Okular 0.6.1 utgiven
---
Den första underhållsutgåvan i KDE 4.0-serien inkluderar Okular 0.6.1. Den innehåller en hel del felrättningar, inklusive bättre inte ladda ner filer igen när de sparas, förbättringar av användbarheten, etc. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
