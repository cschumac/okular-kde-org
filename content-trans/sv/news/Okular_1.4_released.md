---
date: 2018-04-19
title: Okular 1.4 utgiven
---
Version 1.4 av Okular har givits ut tillsammans med utgåva 18.04 av KDE:s program. Utgåvan introducerar förbättrat stöd för Javascript i PDF-filer och stöder att avbryta PDF-återgivning, vilket betyder att om du har en komplicerad PDF-fil och ändrar zoomning medan den återges avbryts det omedelbart istället för att vänta på att återgivningen ska bli klar. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Okular 1.4 är en rekommenderad uppdatering för alla som använder Okular.
