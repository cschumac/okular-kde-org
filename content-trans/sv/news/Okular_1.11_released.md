---
date: 2020-08-13
title: Okular 1.11 utgiven
---
Version 1.11 av Okular har givits ut. Utgåvan introducerar ett nytt användargränssnitt för kommentarer och diverse mindre felrättningar och funktionsförbättringar överallt. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 är en rekommenderad uppdatering för alla som använder Okular.
