---
date: 2011-07-27
title: Okular 0.13 utgiven
---
Version 0.13 av Okular har givits ut tillsammans med utgåva 4.7 av KDE:s program. Utgåvan introducerar mindre rättelser och funktioner och är en rekommenderad uppdatering för alla som använder Okular.
