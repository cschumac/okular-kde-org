---
date: 2017-08-17
title: Publicat l'Okular 1.2
---
S'ha publicat la versió 1.2 de l'Okular conjuntament amb la publicació 17.08 de les Aplicacions del KDE. Aquesta publicació presenta petites esmenes d'errors i millores. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. L'Okular 1.2 és una actualització recomanada per a tothom que usi l'Okular.
