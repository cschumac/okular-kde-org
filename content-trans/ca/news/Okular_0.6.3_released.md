---
date: 2008-04-02
title: Publicat l'Okular 0.6.3
---
La tercera publicació de manteniment de les sèries 4.0 del KDE conté l'Okular 0.6.3. Inclou esmenes d'errors, com una manera millor d'obtenir la posició del text a un document PDF, i un parell d'esmenes del sistema d'anotacions i de la taula de continguts. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
