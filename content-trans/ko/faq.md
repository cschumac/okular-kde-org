---
faq:
- answer: Okular의 우분투(및 쿠분투) 패키지는 이 두 형식을 지원하지 않은 상태로 컴파일됩니다. 그 이유는 [Launchpad 보고서](https://bugs.launchpad.net/kdegraphics/+bug/277007)에
    설명되어 있습니다.
  question: 우분투를 사용하고 있습니다. okular-extra-backends 및 libchm1 패키지가 설치되어 있어도 CHM 및 EPub
    문서를 읽을 수 없습니다. 왜 그런가요?
- answer: 시스템에 음성 합성 서비스가 설치되어 있지 않습니다. Qt Speech 라이브러리를 설치하면 사용할 수 있습니다.
  question: 왜 도구 메뉴의 말하기 옵션이 회색으로 표시되나요?
- answer: poppler-data 패키지를 설치하십시오.
  question: 일부 문자가 표시되지 않으며 디버그를 활성화하면 'XXX 언어의 언어 팩이 없습니다'와 같은 메시지가 표시됩니다
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: 자주 묻는 질문
---
