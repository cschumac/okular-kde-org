---
intro: Okular는 다양한 플랫폼에서 컴파일된 패키지로 게종됩니다. 오른쪽에서 리눅스 배포판별 패키지 상태를 확인할 수 있으며, 다른 운영
  체제의 상황은 이 문서에서 설명합니다
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: 턱스
  name: 리눅스
  text: Okular는 다양한 리눅스 배포판에서 사용할 수 있습니다. [KDE 소프트웨어 센터](https://apps.kde.org/ko/okular)에서
    설치할 수 있습니다.
- image: /images/flatpak.png
  image_alt: Flatpak 로고
  name: Flatpak
  text: Flathub에서 최신 [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)
    패키지를 다운로드할 수 있습니다. Okular 일일 빌드 시험적 Flatpak은 [KDE Flatpak 저장소에서 설치](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    )할 수 있습니다.
- image: /images/ark.svg
  image_alt: Ark 로고
  name: 릴리스 소스
  text: Okular는 KDE 기어의 일부로 정기적으로 출시됩니다. 소스 코드에서 빌드하려면 [빌드](/build-it) 부분을 참조하십시오.
- image: /images/windows.svg
  image_alt: Windows 로고
  name: Windows
  text: Windows에 KDE 소프트웨어를 설치하는 방법은 [KDE on Windows Initiative](https://community.kde.org/Windows)
    페이지를 참조하십시오. 안정 릴리스는 [Windows 스토어](https://www.microsoft.com/store/apps/9N41MSQ1WNM8)</a>에서도
    사용할 수 있습니다. [실험적인 일간 빌드](https://binary-factory.kde.org/job/Okular_Nightly_win64/)</a>도
    있습니다. 일간 빌드를 사용하신다면 테스트와 패치를 기다리고 있습니다.
sassFiles:
- /sass/download.scss
title: 다운로드
---
