---
menu:
  main:
    name: 빌드
    parent: about
    weight: 2
title: 리눅스 환경에서 소스 코드에서 Okular 컴파일
---
<span style="background-color:#e8f4fa">컴파일된 패키지는 [다운로드 페이지](/download/)에서 확인할 수 있습니다. 패키징 상태는 [여기](https://repology.org/project/okular/versions)에서 확인할 수 있습니다</span>

Okular를 직접 컴파일하려면 배포판에서 제공하는 컴파일 환경을 구축해야 합니다. Okular 개발 버전을 컴파일하려면 KDE 커뮤니티 위키에 있는 소스에서 빌드 페이지를 참조하십시오.

다음 방법으로 Okular를 다운로드하고 컴파일 할 수 있습니다:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

Okular를 시스템 설치 디렉터리와 다른 경로에 설치했다면 `source build/prefix.sh; okular` 명령을 실행해야 제대로 된 Okular 인스턴스와 라이브러리를 불러 옵니다.

## 선택적 패키지

Okular에 추가 기능을 제공하는 선택적 패키지를 설치할 수 있습니다. 일부 기능은 배포판에 패키지로 들어 있을 수도 있으며, 일부는 그렇지 않을 수도 있습니다. 문제 발생을 피하려면 배포판 패키지를 사용하십시오

* Poppler(PDF 백엔드): PDF 백엔드를 컴파일하려면 [Poppler 라이브러리](http://poppler.freedesktop.org) 0.24 이상이 필요합니다
* Libspectre: 포스트스크립트(PS) 백엔드를 컴파일하고 사용하려면 libspectre 0.2 이상이 필요합니다. 배포판에 libspectre 패키지가 없거나 패키지 버전이 낮으면 [여기](http://libspectre.freedesktop.org)에서 다운로드할 수 있습니다
* DjVuLibre: DjVu 백엔드를 컴파일하려면 DjVuLibre 3.5.17 이상이 필요합니다. Libspectre처럼 배포판에서 설치하거나 [여기](http://djvulibre.djvuzone.org)에서 다운로드할 수 있습니다.
* libTIFF: TIFF/팩스 지원에 필요합니다. 현재 최소 버전 요구 사항이 없기 때문에 배포판에 대부분 배포판에서 사용할 수 있는 라이브러리 버전으로 컴파일할 수 있어야 합니다. 만약 문제가 있다면 Okular 개발자에게 문의해 주십시오.
* libCHM: CHM 백엔드 컴파일에 필요합니다. libTIFF처럼 최소 버전 요구 사항이 없습니다.
* Libepub: EPub 지원이 필요하면 배포판이나 [SourceForge](http://sourceforge.net/projects/ebook-tools)에서 이 라이브러리를 설치하십시오.
