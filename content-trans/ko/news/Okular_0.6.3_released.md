---
date: 2008-04-02
title: Okular 0.6.3 출시
---
KDE 4.0 시리즈의 세 번째 유지 보수 릴리스에는 Okular 0.6.3이 들어 있습니다. 이 릴리스에는 PDF 문서의 텍스트 위치 획득 개선, 주석 시스템 및 목차 개선 등 여러 버그 수정이 들어 있습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a> 페이지를 방문하십시오
