---
date: 2012-10-10
title: Okular 포럼
---
이제 KDE 커뮤니티 포럼 내에 하위 포럼이 생겼습니다. <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>에서 확인하십시오.
