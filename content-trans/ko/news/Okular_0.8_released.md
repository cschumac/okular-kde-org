---
date: 2009-01-27
title: Okular 0.8 출시
---
Okular 팀은 <a href="http://www.kde.org/announcements/4.2/">KDE 4.2의</a>일부로 새로운 Okular 버전을 발표합니다. 주요 새로운 기능과 개선 사항은 다음과 같습니다(별도의 순서 없음):<ul type="disc"><li>문서 보관 기능 추가</li><li>현재 쪽부터 검색</li><li>PDF 문서에 포함된 동영상 초기 지원</li><li>프레젠테이션 모드 개선: 화면 보호기 진입 방지 및 검은 화면 모드</li><li>더 많은 스탬프 주석</li><li>Lilypond의 "포인트 앤 클릭" 링크 지원</li><li>역탐색 편집기 설정</li><li>ODT, Fictionbook, EPub 백엔드에서 OpenDocument 텍스트 및 HTML로 내보내기 지원(Qt 4.5 필요)</li><li>G3/G4 팩스 문서용 새로운 백엔드</li><li>DjVu 백엔드: 메모리 관리 개선</li><li>OpenDocument Text 백엔드: 다양한 개선</li> <li>... 그 외 여러 작은 기능 및 수정 사항</li> </ul>물론 여기에 언급하지 않은 많은 버그가 수정되었습니다.
