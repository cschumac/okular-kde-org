---
date: 2020-12-10
title: Okular 20.12 출시
---
Okular 20.12 버전이 출시되었습니다. 이 릴리스에는 다양한 기능 개선과 수정이 있었습니다. 전체 변경 내역은 <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a> 페이지에서 확인할 수 있습니다. Okular 20.12는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
