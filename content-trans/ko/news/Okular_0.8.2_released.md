---
date: 2009-04-02
title: Okular 0.8.2 출시
---
KDE 4.2 시리즈의 두 번째 유지 관리 릴리스에는 Okular 0.8.2가 들어 있습니다. 이 릴리스에는 DVI 및 pdfsync 역탐색 지원 개선, 프레젠테이션 모드 개선이 들어 있습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a> 페이지를 방문하십시오
