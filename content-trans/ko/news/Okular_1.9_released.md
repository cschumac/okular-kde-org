---
date: 2019-12-12
title: Okular 1.9 출시
---
Okular 1.9 버전이 출시되었습니다. 이 릴리스에는 ComicBook 압축 파일에 대한 cb7 지원, 다양한 수정 및 작은 기능 중에서 PDF 파일에 대한 향상된 자바스크립트지원이 도입되었습니다. 전체 변경 내역은 <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a> 페이지에서 확인할 수 있습니다. Okular 1.9는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
