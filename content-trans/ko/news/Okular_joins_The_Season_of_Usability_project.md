---
date: 2007-01-31
title: Okular가 Season of Usability 프로젝트에 참여함
---
Okular 팀은 <a href="http://www.openusability.org">OpenUsability</a>의 사용성 전문가가 운영하는 <a href="http://www.openusability.org/season/0607/">Season of Usability</a> 프로젝트에 참여하는 프로그램으로 선정된 것을 알립니다. Sharad Baliyan이 팀에 참여하게 된 것을 환영하며, Florian Graessle와 Pino Toscano의 Okular 개선을 위한 지속적인 노력에 감사를 표합니다.
