---
intro: Okular는 다양한 문서 형식과 용도를 지원합니다. 이 페이지는 항상 Okular의 안정 버전(현재 Okular 20.12)의 정보를
  표시합니다
layout: formats
menu:
  main:
    name: 문서 형식
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: 문서 형식 처리기 상태
---
