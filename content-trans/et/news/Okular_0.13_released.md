---
date: 2011-07-27
title: Ilmus Okular 0.13
---
Okulari versioon 0.13 ilmus koos KDE rakenduste väljalaskega 4.7. See väljalase sisaldab väiksemaid parandusi ja uusi võimalusi ning kõigil, kes kasutavad Okulari, on soovitatav see kasutusele võtta.
