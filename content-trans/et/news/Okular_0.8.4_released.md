---
date: 2009-06-03
title: Ilmus Okular 0.8.4
---
KDE 4.2 seeria neljas hooldusväljalase sisaldab Okulari 0.8.4. See sisaldab mõningaid parandusi OpenDocumenti tekstidokumentide jaoks, mõne krahhi parandamist ja väiksemaid veaparandusi liideses. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
