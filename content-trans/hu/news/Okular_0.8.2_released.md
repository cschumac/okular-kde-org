---
date: 2009-04-02
title: Megjelent az Okular 0.8.2
---
The second maintenance release of the KDE 4.2 series includes Okular 0.8.2. It includes better support (hopefully working) for DVI and pdfsync inverse search, and fixes and small improvements in the presentation mode. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
