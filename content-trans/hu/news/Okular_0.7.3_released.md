---
date: 2008-11-04
title: Megjelent az Okular 0.7.3
---
The third maintenance release of the KDE 4.1 series includes Okular 0.7.3. It includes some minor fixes in the user interface and in the text search. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
