---
date: 2020-04-23
title: Publicat l'Okular 1.10
---
S'ha publicat la versió 1.10 de l'Okular. Aquesta publicació presenta el desplaçament cinètic, millores a la gestió de les pestanyes, millores a la IU mòbil i diverses correccions i característiques secundàries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. L'Okular 1.10 és una actualització recomanada per a tothom que usi l'Okular.
