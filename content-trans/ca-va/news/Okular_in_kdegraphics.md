---
date: 2007-04-04
title: L'Okular al «kdegraphics»
---
L'equip de l'Okular anuncia amb satisfacció que l'Okular ara és part del mòdul «kdegraphics». La propera versió de l'Okular es distribuirà amb el KDE 4.0.
