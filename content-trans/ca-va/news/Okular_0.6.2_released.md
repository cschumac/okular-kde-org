---
date: 2008-03-05
title: Publicat l'Okular 0.6.2
---
La segona publicació de manteniment de les sèries 4.0 del KDE conté l'Okular 0.6.2. Inclou diverses esmenes d'errors, incloent més estabilitat en tancar un document, i petites esmenes al sistema de punts. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
