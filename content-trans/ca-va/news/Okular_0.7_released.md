---
date: 2008-07-29
title: Publicat l'Okular 0.7
---
L'equip de l'Okular anuncia amb satisfacció una versió nova de l'Okular, publicada com a part del <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Entre les característiques i millores noves s'inclouen (en ordre aleatori):

* Possibilitat de guardar un document PDF amb els canvis als camps de formulari
* Mode presentació: permet més d'una pantalla, i possibilitat de desactivar les transicions
* Ara el nivell de zoom s'emmagatzema per a cada document
* S'ha ampliat el suport per al text-a-veu, que es pot usar per a llegir el document sencer, una pàgina específica o una selecció de text
* Direcció cap arrere per a la cerca de text
* Suport millorat per als formularis
* Suport preliminar (realment bàsic i probablement incomplet) per al JavaScript als documents PDF
* Suport per a anotacions adjuntes als fitxers
* Possibilitat d'eliminar la vora blanca de les pàgines en visualitzar pàgines
* Dorsal nou per a documents EPub
* Dorsal de text en OpenDocument: permet documents encriptats, millores a les llistes i taules
* Dorsal XPS: diverses millores en carregar i renderitzar, ara hauria de ser molt més usable
* Dorsal PS: millores diverses, principalment gràcies a la «libspectre» requerida més nova
