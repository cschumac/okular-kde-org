---
date: 2014-12-17
title: Publicat l'Okular 0.21
---
S'ha publicat la versió 0.21 de l'Okular conjuntament amb la publicació 14.12 de les Aplicacions del KDE. Aquesta publicació presenta característiques noves com la cerca inversa «latex-synctex» als DVI i petites esmenes d'errors. L'Okular 0.21 és una actualització recomanada per a tothom que usi l'Okular.
