---
date: 2016-12-15
title: Publicat l'Okular 1.0
---
S'ha publicat la versió 1.0 de l'Okular conjuntament amb la publicació 16.12 de les Aplicacions del KDE. Aquesta publicació està basada ara amb els Frameworks 5. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. L'Okular 1.0 és una actualització recomanada per a tothom que usi l'Okular.
