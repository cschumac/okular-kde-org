---
date: 2010-02-09
title: Okular 0.10 yayımlandı
---
Okular'ın 0.10 sürümü KDE 4.4 yazılım derlemesiyle birlikte yayımlandı. Kararlılıktaki genel iyileştirmelerin yanında bu sürüme, yeni/geliştirilmiş ters ve ileri arama özelliği ile dvi ve pdf dosyalarının lateks kaynak kod satırlarına bağlı ilgili yerlerde arama desteği eklenmiştir...
