---
date: 2008-11-04
title: Okular 0.7.3 yayımlandı
---
KDE 4.1 serisinin üçüncü bakım sürümü Okular 0.7.3'ü içeriyor. Bu sürüm kullanıcı arayüzünde ve metin aramada bazı küçük düzeltmeleri içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a> adresinde okuyabilirsiniz.
