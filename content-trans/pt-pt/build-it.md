---
menu:
  main:
    name: Compilá-lo
    parent: about
    weight: 2
title: Compilar o Okular a partir do código no Linux
---
<span style="background-color:#e8f4fa">Se estiver à procura dos pacotes pré-compilados, visite a [página de transferências](/download/). Poderá ver o estado dos pacotes [aqui](https://repology.org/project/okular/versions)</span>

Se quiser compilar o Okular, terá de ter um ambiente de compilação preparado, o qual normalmente deverá ser fornecido pela sua distribuição. No caso de querer compilar a versão de desenvolvimento do Okular, veja por favor em 'Compilar a partir do código' na Wiki da Comunidade do KDE.

Pode obter e compilar o Okular desta forma:

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/local/da/sua/pasta/instalacao ..`
6. `make`
7. `make install`

Se instalar o Okular numa localização diferente da pasta de instalação do seu sistema, é possível que tenha de executar `source build/prefix.sh; okular` para que sejam escolhidas as versões correctas do Okular e das bibliotecas.

## Pacotes opcionais

Existem alguns pacotes opcionais que poderá instalar para ter algumas outras funcionalidades no Okular. Alguns poderão já ter sido criados para a sua distribuição, mas outros não. Se quiser evitar problemas, use os pacotes suportados pela sua distribuição

* Poppler (infra-estrutura de PDF): Para compilar a infra-estrutura de PDF, precisa da [biblioteca Poppler](http://poppler.freedesktop.org). A versão mínima obrigatória é a 0.24
* Libspectre: Para compilar e usar esta infra-estrutura de PostScript (PS), precisa da libspectre >= 02. Se a sua distribuição não tiver o pacote respectivo, ou se a versão do pacote não for a suficiente, podê-lo-á obter [aqui](http://libspectre.freedesktop.org)
* DjVuLibre: Para compilar a infra-estrutura de DjVu, precisa da DjVuLibre >= 3.5.17. Como na Libspectre, poderá obter a mesma a partir da sua distribuição ou [aqui](http://djvulibre.djvuzone.org).
* libTIFF: A libTIFF é necessária para compilar a infra-estrutura de TIFF/fax. De momento, não existe nenhuma versão mínima necessária, por isso qualquer versão relativamente recente da biblioteca que estiver disponível na sua distribuição deverá funcionar. No caso de ter problemas com isso, não hesite em contactar a equipa de desenvolvimento do Okular.
* libCHM: Esta é necessária para compilar a infra-estrutura de CHM. Tal como na libTIFF, não existe nenhum requisito de versão mínima
* Libepub: Se precisar do suporte para EPub, poderá instalar esta biblioteca da sua distribuição ou a partir do [sourceforge](http://sourceforge.net/projects/ebook-tools).
