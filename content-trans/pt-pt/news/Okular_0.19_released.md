---
date: 2014-04-16
title: O Okular 0.19 foi lançado
---
A versão 0.19 do Okular foi lançada em conjunto com a versão 4.13 das Aplicações do KDE. Esta versão introduz novas funcionalidades, como o suporte de páginas na interface, a utilização da resolução em PPP's do monitor para que o tamanho da página corresponda ao tamanho do papel real, melhorias na funcionalidade para Desfazer/Refazer, assim como outras melhorias menores. É uma actualização recomendada para todos os que usam o Okular.
