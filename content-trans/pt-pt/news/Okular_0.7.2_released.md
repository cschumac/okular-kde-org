---
date: 2008-10-03
title: O Okular 0.7.2 foi lançado
---
A segunda versão de manutenção da série KDE 4.1 inclui o Okular 0.7.2. Inclui algumas pequenas correcções nas infra-estruturas TIFF e Comicbook e a alteração para o "Ajustar à Largura" como ampliação por omissão. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
