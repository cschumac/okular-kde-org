---
date: 2007-04-04
title: Okular no kdegraphics
---
A equipa do Okular está orgulhosa de anunciar que o Okular faz agora parte do módulo 'kdegraphics'. A próxima versão do Okular irá sair com o KDE 4.0.
