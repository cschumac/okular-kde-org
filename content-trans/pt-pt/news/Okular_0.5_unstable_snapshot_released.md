---
date: 2006-08-27
title: A versão instável do Okular 0.5 foi lançada
---
A equipa do Okular orgulha-se de anunciar o lançamento de uma versão do Okular que compila contra a <a href="http://dot.kde.org/1155935483/">versão 'Krash' do KDE 4</a>. Esta imagem ainda não está completamente funcional, dado que ainda temos montes de coisas para polir e terminar, mas está completamente à vontade para a testar e nos dar o máximo de comentários que desejar. Poderá encontrar o pacote instável em <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Dê uma vista de olhos na página de <a href="download.php">transferências</a> para garantir que tem todas as bibliotecas necessárias.
