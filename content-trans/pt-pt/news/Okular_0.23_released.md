---
date: 2015-08-19
title: O Okular 0.23 foi lançado
---
A versão 0.23 do Okular foi lançada em conjunto com a versão 15.08 das Aplicações do KDE. Esta versão introduz o suporte para a transição por Desvanecimento no modo de apresentação, assim como algumas correcções de erros que dizem respeito às anotações e à reprodução de vídeo. É uma actualização recomendada para todos os que usam o Okular.
