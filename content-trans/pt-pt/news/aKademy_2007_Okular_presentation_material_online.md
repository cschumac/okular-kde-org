---
date: 2007-07-10
title: Material 'online' de apresentações do Okular no aKademy 2007
---
A apresentação do Okular, dada pelo Pino Toscano na <a href="http://akademy2007.kde.org">aKademy 2007</a> está agora disponível. Existem tanto <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">'slides'</a> como um <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">vídeo</a> disponíveis.
