---
date: 2019-12-12
title: O Okular 1.9 foi lançado
---
A versão 1.9 do Okular foi lançada. Esta versão introduz o suporte para o CB7 dos arquivos de BD, o suporte de JavaScript melhorado para os ficheiros PDF, entre outras pequenas correcções de erros e funcionalidades. Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=19.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=19.12.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
