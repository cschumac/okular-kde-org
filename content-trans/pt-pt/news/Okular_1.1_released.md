---
date: 2017-04-20
title: O Okular 1.1 foi lançado
---
A versão 1.1 do Okular foi lançada em conjunto com a versão 17.04 das Aplicações do KDE. Esta versão introduz a funcionalidade de dimensionamento das anotações, o suporte para o cálculo automático do conteúdo dos formulários, melhorias para ecrãs tácteis, entre outras coisas! Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. O Okular 1.1 é uma actualização recomendada para todos os que usam o Okular.
