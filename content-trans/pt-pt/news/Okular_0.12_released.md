---
date: 2011-01-26
title: O Okular 0.12 foi lançado
---
A versão 0.12 do Okular foi lançada em conjunto com a versão 4.6 das Aplicações do KDE. Esta versão introduz algumas pequenas correcções e funcionalidades, e é uma actualização recomendada para todos os que usam o Okular.
