---
date: 2007-01-31
title: O Okular junta-se ao projecto Season of Usability
---
A equipa do Okular está orgulhosa em anunciar que o Okular foi uma das aplicações seleccionadas para participar no projecto <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, efectuado por peritos em usabilidade da <a href="http://www.openusability.org">OpenUsability</a>. A partir daqui, desejamos as boas-vindas à equipa para Sharad Baliyan e muito obrigado a Florian Graessle e a Pino Toscano pelo seu trabalho continuado na melhoria do Okular.
