---
date: 2012-10-10
title: Fóruns do Okular
---
Agora temos um sub-fórum dentro dos Fóruns da Comunidade do KDE. Podê-lo-á encontrar em <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.
