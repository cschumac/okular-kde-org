---
faq:
- answer: Os pacotes do Ubuntu (e do Kubuntu também) para o Okular são compilados
    sem o suporte para estes dois formatos. A razão está explicada [neste](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    relatório do Launchpad.
  question: Ao usar o Ubuntu, não consigo ler documentos CHM e EPub, mesmo que tenha
    o 'okular-extra-backends' e o 'libchm1' instalados. Porquê?
- answer: Dado que não tem um serviço de fala no seu sistema, instale a biblioteca
    QtSpeech para que os mesmos sejam activados
  question: Porque é que as opções de fala no menu Ferramentas estão cinzentas?
- answer: Instalar o pacote 'poppler-data'
  question: Alguns caracteres não são desenhados e, ao activar a depuração, algumas
    linhas indicam 'Falta o pacote da língua xxx'
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Perguntas Mais Frequentes
---
