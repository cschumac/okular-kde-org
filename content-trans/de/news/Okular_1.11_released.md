---
date: 2020-08-13
title: Okular 1.11 veröffentlicht
---
Die Version 1.11 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält eine neue Benutzeroberfläche für Anmerkungen sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
