---
date: 2018-12-13
title: Okular 1.6 veröffentlicht
---
Die Version 1.6 von Okular wurde zusammen mit den KDE-Anwendungen 18.12 veröffentlicht. Diese Veröffentlichung enthält die neuer Anmerkung Textkommentar sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0#okular</a>. Okular 1.6 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
