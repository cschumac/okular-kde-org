---
date: 2009-12-01
title: Okular 0.9.4 veröffentlicht
---
Die vierte Wartungsversion der Reihe KDE 4.3 enthält Okular 0.9.4. Diese Version bringt Korrekturen, die Abstürze verhindern und behebt ein paar Fehler in der Benutzeroberfläche. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">hier</a>.
