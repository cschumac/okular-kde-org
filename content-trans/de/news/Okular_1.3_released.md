---
date: 2017-12-14
title: Okular 1.3 veröffentlicht
---
Die Version 1.3 von Okular wurde zusammen mit den KDE-Anwendungen 17.12 veröffentlicht. In dieser Veröffentlichung wurde das Speichern von Anmerkungen und Formulardaten geändert, Aktualisierungen der Anzeige für große Dateien erfolgen inkrementell und Textverknüpfungen sind interaktiv. Zum Menü Datei wurde eine Aktionen zum Veröffentlichen hinzugefügt, Markdown wird jetzt unterstützt und einige Probleme mit HiDPI-Anzeigen wurden gelöst. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
