---
date: 2018-08-16
title: Okular 1.5 veröffentlicht
---
Die Version 1.5 von Okular wurde zusammen mit den KDE-Anwendungen 18.08 veröffentlicht. Diese Veröffentlichung enthält Verbesserungen beim Bearbeiten von Formularen sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
