---
date: 2007-04-04
title: Okular in kdegraphics
---
Das Okular-Team freut sich, ankündigen zu können, dass Okular jetzt Teil des Moduls „kdegraphics“ ist. Die nächste Version von Okular wird zusammen mit KDE 4.0 ausgeliefert.
