---
date: 2018-04-19
title: Okular 1.4 veröffentlicht
---
Die Version 1.4 von Okular wurde zusammen mit den KDE-Anwendungen 18.04 veröffentlicht. In dieser Veröffentlichung wurde die Unterstützung für JavaScript in PDF-Dateien verbessert, das Rendern von PDF kann abgebrochen werden, d. h. wenn Sie bei komplexen PDFs die Vergrößerung beim Rendern ändern, dann wird das Rendern sofort abgebrochen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.4 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
