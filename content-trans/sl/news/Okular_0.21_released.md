---
date: 2014-12-17
title: Izšel je Okular 0.21
---
Različna različica programa Okular je bila izdana skupaj s programi KDE verzije 14.12. Ta izdaja uvaja nove možnosti, kot je latex-synctex povratno iskanje v dvi in majhne popravke. Okular 0.21 je priporočen je za vse, ki uporabljajo Okular.
