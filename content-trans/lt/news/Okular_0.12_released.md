---
date: 2011-01-26
title: Okular 0.12 išleistas
---
The 0.12 version of Okular has been released together with KDE Applications 4.6 release. This release introduce small fixes and features and is a recommended update for everyone using Okular.
