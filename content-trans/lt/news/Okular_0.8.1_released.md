---
date: 2009-03-04
title: Okular 0.8.1 išleistas
---
The first maintenance release of the KDE 4.2 series includes Okular 0.8.1. It includes some crash fixes in the CHM and DjVu backends, and minor fixes in the user interface. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
