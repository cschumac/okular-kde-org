---
date: 2020-12-10
title: Okular 20.12 发布
---
The 20.12 version of Okular has been released. This release introduces various minor fixes and features all over. You can check the full changelog at <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 is a recommended update for everyone using Okular.
