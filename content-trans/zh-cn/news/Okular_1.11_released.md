---
date: 2020-08-13
title: Okular 1.11 发布
---
The 1.11 version of Okular has been released. This release introduces a new annotation user interface and various minor fixes and features all over. You can check the full changelog at <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 is a recommended update for everyone using Okular.
