---
date: 2015-12-16
title: Okular 0.24 已发布
---
The 0.24 version of Okular has been released together with KDE Applications 15.12 release. This release introduces minor bugfixes and features, you can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 is a recommended update for everyone using Okular.
