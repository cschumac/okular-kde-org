---
date: 2018-12-13
title: Okular 1.6 已发布
---
The 1.6 version of Okular has been released together with KDE Applications 18.12 release. This release introduces the new Typewriter annotation tool among various other fixes and small features. You can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Okular 1.6 is a recommended update for everyone using Okular.
