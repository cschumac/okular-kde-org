---
faq:
- answer: Ubuntu (Kubuntu 也一样) 在构建它们的 Okular 软件包时并未启用这两种格式的支持。要了解详情，请查看[这篇程序缺陷报告](https://bugs.launchpad.net/kdegraphics/+bug/277007)。
  question: 我使用的是 Ubuntu 系统，我无法使用 Okular 打开 CHM 和 EPub 文档，而我已经安装了 okular-extra-backends
    和 libchm1。怎么办？
- answer: 因为您的系统中并未安装语音转换服务。请先安装 Qt Speech 程序库，然后此功能就能启用了。
  question: 为何工具菜单的朗读选项不可用？
- answer: 安装 poppler-data 软件包
  question: 某些字符无法渲染。在启用调试模式后，某些段落会显示“缺少 xxx 的语言包”
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: 常见问题解答
---
