---
menu:
  main:
    name: 构建指南
    parent: about
    weight: 2
title: 在 Linux 上从源代码编译 Okular
---
<span style="background-color:#e8f4fa">如果您想要使用预编译可执行软件包，请前往[下载页面](/download/)。请[在此页面](https://repology.org/project/okular/versions)查看各 Linux 发行版的 Okular 软件包状态</span>

如果您想要编译 Okular，您必须设置它的编译环境。您的发行版通常已经提供了相关环境。如果您想要编译 Okular 的开发版本，请按照 Okular 的社区百科页面的相关章节进行操作。

下载和编译 Okular 的指令序列：

1. `git clone https://invent.kde.org/graphics/okular.git`
2. `cd okular`
3. `mkdir build`
4. `cd build`
5. `cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..`
6. `make`
7. `make install`

如果您的 Okular 安装路径与系统安装路径不同，您可能需要运行 `source build/prefix.sh; okular` 指令，这样系统才能正确识别 Okular 实例和相关程序库。

## 可选软件包

您可以安装额外的软件包来扩展 Okular 的功能。您的发行版可能已经提供了这些软件包，但有些发行版则不一定。如果您不想遇到问题，请使用您的发行版提供的软件包。

* Poppler (PDF 后端程序)：要编译 Okular 的 PDF 后端程序，您需要使用 [Poppler 程序库](http://poppler.freedesktop.org) 0.24 及以上版本。
* Libspectre：要编译 Okular 的 PostScipt (PS) 后端程序，您需要使用 libspectre 0.2 及以上版本。如果您的发行版没有提供它，或者版本过低，您可以从[此页面进行下载](http://libspectre.freedesktop.org)
* DjVuLibre：要编译 Okular 的 DjVu 后端程序，您需要使用 DjVuLibre 3.5.17 及以上版本。您可以直接从发行版安装，也可以[从此网站下载](http://djvulibre.djvuzone.org)。
* libTIFF：用于为 Okular 提供 TIFF 和 fax 支持。Okular 目前对该程序库的版本没有要求，您发行版提供的版本应该能正常工作。如果遇到了问题，请即使联系 Okular 的开发人员。
* libCHM：用于编译 CHM 后端程序。和 libTIFF 一样，Okular 对该程序库版本没有要求
* Libepub：为 Okular 提供 EPub 格式支持。您可以从发行版安装它，也可以从[该项目的 sourceforge 网站](http://sourceforge.net/projects/ebook-tools)下载。
