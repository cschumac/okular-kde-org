---
menu:
  main:
    parent: about
    weight: 4
title: 联系我们
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

您可以通过多种方式联系 Okular 团队。

* 邮件列表：我们使用位于 kde.org 网站的 [okular-devel 邮件列表](https://mail.kde.org/mailman/listinfo/okular-devel)协调开发工作。您可以在该邮件列表讨论 Okular 核心程序的开发工作、反馈已有后端程序的问题，或者提出新后端程序支持。

* IRC：一般聊天请在 Okular 的 IRC 频道 [#okular](irc://irc.kde.org/#okular) 和位于 [Freenode network](http://www.freenode.net/) 的 [#kde-devel](irc://irc.kde.org/#kde-devel) 频道进行，一些 Okular 开发人员也会在这些地方出没。

* Matrix：上面的聊天室也可以通过 Matrix 网络的 [#okular:kde.org](https://matrix.to/#/#okular:kde.org) 频道进行访问。

* 论坛：如果您习惯使用论坛，可以前往 [Okular 论坛](http://forum.kde.org/viewforum.php?f=251)。它是 [KDE 社区论坛](http://forum.kde.org/)的一个子板块。

* 提交程序缺陷和需求：请在 [KDE 程序缺陷跟踪网站](https://bugs.kde.org/)反馈程序缺陷和提交需求。如果您需要帮助，你可以在 [Okular 社区百科页面](https://community.kde.org/Okular)查看最重要的程序缺陷列表。
