---
intro: Okular 支持多种文档格式和相关操作。此页面反映了 Okular 稳定版本 (当前为 20.12) 的兼容性状态。
layout: formats
menu:
  main:
    name: 文件格式
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: 文档格式处理器兼容性
---
