---
intro: Okular 为多种操作系统平台提供预编译的可执行软件包。各 Linux 发行版的 Okular 软件包状态显示在本页右侧，您也可以下载其他版本。
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: 绝大多数 Linux 发行版已经提供了 Okular。你可以使用系统自带的软件中心或者 [KDE 软件中心](https://apps.kde.org/okular)进行安装。
- image: /images/flatpak.png
  image_alt: Flatpak 标志
  name: Flatpak
  text: 你可以从 Flathub 安装最新版本的 [Okular Flatpak](https://flathub.org/apps/details/org.kde.okular)。实验性的
    Okular 每日构建测试版可从[KDE Flatpak 软件仓库](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    )进行安装。
- image: /images/ark.svg
  image_alt: Ark 图标
  name: 最新版本源代码
  text: Okular 作为 KDE Gear 的一部分定期发布新版。如果您想要从源代码进行构建，请访问[构建指南](/build-it)。
- image: /images/windows.svg
  image_alt: Windows 标志
  name: Windows
  text: 要获取在 Windows 上安装 KDE 软件的信息，请查看 [KDE Windows 支持页面](https://community.kde.org/Windows)。Okular
    的稳定版本可从 [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8) 下载安装。如果您想要测试新功能并报告程序缺陷，请使用实验性的
    [每日构建测试版](https://binary-factory.kde.org/job/Okular_Nightly_win64/)。
sassFiles:
- /sass/download.scss
title: 下载
---
