---
date: 2010-02-09
title: Okular 0.10 lansat
---
The 0.10 version of Okular has been released together with KDE SC 4.4. Besides general improvements in stability this release sports new/improved support for both inverse search and forward search linking latex source code lines with the corresponding locations in dvi and pdf files.
