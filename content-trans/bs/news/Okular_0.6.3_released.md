---
date: 2008-04-02
title: Okular 0.6.3 pušten
---
Treće izdanje KDE 4.0 serije uključuje Okular 0.6.3. Ono uključuje nekoliko popravki, npr. bolje pozicioniranje teksta u PDF dokumentima, i nekoliko popravki za bilješke sistema i tablicu sadržaja. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
