---
date: 2008-03-05
title: Okular 0.6.2 pušten
---
Drugo izdanje KDE 4.0 serije uključuje Okular 0.6.2. Ono uključuje nekoliko ispravljenih grešaka, uključujući veću stabilnost prilikom zatvaranja dokumenata i manje popravke za oznaku sistema. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
