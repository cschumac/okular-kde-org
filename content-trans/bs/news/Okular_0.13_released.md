---
date: 2011-07-27
title: Okular 0.13 pušten
---
Verzija 0.13 Okulara je puštena zajedno sa KDE 4.7 aplikacijom. Ovo izdanje predstavlja mala poboljšanja i nove mogućnosti i  preporučena je nadogradnja za sve korisnike Okulara.
