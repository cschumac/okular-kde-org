---
date: 2013-08-16
title: Okular 0.17 julkaistiin
---
Okularin versio 0.17 on julkaistu KDE:n sovellusten 4.11-julkaisuversiossa. Tämä julkaisuversio esittelee uusia ominaisuuksia kuten kumoa/tee uudelleen -tuen lomakkeille sekä merkinnöille ja tarkastelutyökalujen muuttamismahdollisuuden. Okular 0.17 on suositeltu päivitys kaikille Okularin käyttäjille.
