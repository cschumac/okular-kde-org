---
date: 2008-09-03
title: Okular 0.7.1 julkaistiin
---
Okular 0.7.1 on julkaistu KDE 4.1-sarjan ensimmäisessä korjausjulkaisussa. Tähän julkaisuversioon kuuluu joitakin kaatumiskorjauksia sekä muita pieniä korjauksia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
