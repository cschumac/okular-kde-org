---
date: 2010-08-10
title: Okular 0.11 julkaistiin
---
Okularin versio 0.11 on julkaistu KDE:n sovellusten 4.5-julkaisuversiossa. Tämä julkaisuversio esittelee pieniä korjauksia sekä ominaisuuksia, ja on suositeltu päivitys kaikille Okularin käyttäjille.
