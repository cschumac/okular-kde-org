---
date: 2008-11-04
title: Okular 0.7.3 julkaistiin
---
Okular 0.7.3 on julkaistu KDE 4.1-sarjan kolmannessa korjausjulkaisussa. Tähän julkaisuversioon kuuluu pieniä käyttöliittymä- ja tekstihakukorjauksia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
