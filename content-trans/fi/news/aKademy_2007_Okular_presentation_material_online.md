---
date: 2007-07-10
title: aKademy 2007:ssä pidetyn Okularia koskevan esityksen materiaalit verkossa
---
The Okular talk given by Pino Toscano at <a href="http://akademy2007.kde.org">aKademy 2007</a> is now online. There are both <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">slides</a> and <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a> available.
