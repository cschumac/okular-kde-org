---
date: 2008-04-02
title: Okular 0.6.3 julkaistiin
---
Okular 0.6.3 on julkaistu KDE 4.0-sarjan kolmannessa korjausjulkaisussa. Tähän julkaisuversioon kuuluu muutamia virhekorjauksia kuten parempi tapa saada tekstin sijainti PDF-asiakirjassa ja muutamia korjauksia merkintäjärjestelmään sekä sisällysluetteloon. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
