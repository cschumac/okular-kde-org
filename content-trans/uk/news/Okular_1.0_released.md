---
date: 2016-12-15
title: Випущено Okular 1.0
---
Разом з набором програм KDE 16.12 випущено версію 1.0 Okular. Цей випуск засновано на KDE Frameworks 5. Із повним списком змін можна ознайомитися <a href='https://www.kde.org/announcements/fulllog_applications-16.12.0.php#okular'>тут</a>. Рекомендуємо оновитися до версії 1.0 усім, хто користується Okular.
