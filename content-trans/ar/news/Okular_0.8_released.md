---
date: 2009-01-27
title: صدر أوكلار 0.8
---
The Okular team is proud to announce a new version of Okular, released as part of <a href="http://www.kde.org/announcements/4.2/">KDE 4.2</a>. Some of the new features and improvements include (in random order): <ul type="disc"> <li>Add a Document Archiving possibility</li> <li>Start a search from the current page</li> <li>Preliminar support for videos in PDF documents</li> <li>Presentation mode improvements: screen saver inhibition, and black screen mode</li> <li>More stamp annotations</li> <li>Support for Lilypond's "point-and-click" links</li> <li>Editor configuration for inverse search</li> <li>Export to OpenDocument Text and HTML for ODT, Fictionbook and EPub backends (requires Qt 4.5)</li> <li>New backend for G3/G4 fax documents</li> <li>DjVu backend: memory management improvements</li> <li>OpenDocument Text backend: various improvements</li> <li>... other small features and fixes</li> </ul> And, of course, many bugs were fixed.
