---
intro: Okular reconoce una amplia variedad de formatos de documento y casos de uso.
  Esta página se refiere siempre a la versión estable de Okular, que en la actualidad
  es Okular 20.12.
layout: formats
menu:
  main:
    name: Formato de documentos
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Estado de los gestores de formatos de documentos
---
