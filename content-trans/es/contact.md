---
menu:
  main:
    parent: about
    weight: 4
title: Contacto
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, la mascota de KDE" style="height: 200px;"/>

Puede contactar con el equipo de Okular de diversas maneras:

* Lista de distribución: Para coordinar el desarrollo de Okular usamos la [lista de distribución okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) en kde.org. Puede usarla para hablar sobre el desarrollo de la aplicación principal, y se agradece cualquier observación sobre motores nuevos o sobre los existentes.

* IRC: Para conversaciones generales usamos IRC [#okular](irc://irc.kde.org/#okular) y [#kde-devel](irc://irc.kde.org/#kde-devel) en la [red Freenode](http://www.freenode.net/). Ahí puede encontrar ocasionalmente a algunos de los desarrolladores de Okular.

* Matrix: También puede acceder a la sala de charla anterior sobre la red Matrix usando [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Foro: Si prefiere usar un foro, puede visitar el [foro de Okular](http://forum.kde.org/viewforum.php?f=251) dentro de los [Foros de la comunidad KDE](http://forum.kde.org/).

* Errores y peticiones: Los errores y las peticiones se deben notificar en el [sistema de seguimiento de fallos de KDE](https://bugs.kde.org/). Si desea ayudarnos, puede encontrar la lista de los fallos más importantes [aquí](https://community.kde.org/Okular).
