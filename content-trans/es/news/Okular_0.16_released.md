---
date: 2013-02-06
title: Okular 0.16 publicado
---
La versión 0.16 de Okular ha sido publicada junto con el lanzamiento de las aplicaciones de KDE 4.10. Esta versión introduce nuevas características, como la posibilidad de ampliación a mayores niveles en documentos PDF, un visor basado en Active para dispositivos de tipo tablet, mejor implementación de vídeos en PDF, el área de visión sigue a la selección y mejoras en la edición de notas. Okular 0.16 es una actualización recomendada para todos los usuarios de Okular.
