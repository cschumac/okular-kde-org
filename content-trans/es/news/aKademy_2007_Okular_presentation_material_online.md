---
date: 2007-07-10
title: Material en línea de la presentación de Okular en aKademy 2007
---
La charla sobre Okular dada por Pino Toscano en la <a href="http://akademy2007.kde.org">aKademy 2007</a> está disponible en Internet. Hay una <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">presentación</a> y <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">vídeo</a> disponibles.
