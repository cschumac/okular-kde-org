---
date: 2008-07-29
title: Okular 0.7 publicado
---
El equipo de Okular tiene el placer de anunciar una nueva versión de Okular, publicada como parte de <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>. Entre las nuevas funcionalidades y mejoras se incluyen (en orden aleatorio):

* Posibilidad de guardar un documento PDF con los cambios en los campos del formulario
* Modo presentación: permite usar más de una pantalla y tiene la posibilidad de desactivar las transiciones
* El nivel de ampliación se guarda ahora para cada documento
* Soporte avanzado de texto a voz, que se puede usar para leer la totalidad del documento, una determinada página o el texto seleccionado
* Retroceder en la dirección de búsqueda de texto
* Soporte mejorado de formularios
* Soporte preliminar (realmente básico y muy probablemente incompleto) para JavaScript en documentos PDF
* Soporte para anotaciones de archivos adjuntos
* Posibilidad de eliminar el borde blanco de las páginas al visualizarlas
* Nuevo motor para documentos EPub
* Motor de texto OpenDocument: permite documentos cifrados, mejoras en listas y tablas
* Motor XPS: diversas mejoras de carga y visualización, debería ser mucho más estable ahora
* Motor PS: diversas mejoras, debidas principalmente a que se requiere una versión más moderna de «libspectre»
