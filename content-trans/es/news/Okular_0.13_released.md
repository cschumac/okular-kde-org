---
date: 2011-07-27
title: Okular 0.13 publicado
---
La versión 0.13 de Okular ha sido publicada junto con el lanzamiento de las aplicaciones de KDE 4.7. Esta versión incluye correcciones y características menores, y es una actualización recomendada para todos los usuarios de Okular.
