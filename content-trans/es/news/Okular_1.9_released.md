---
date: 2019-12-12
title: Okular 1.9 publicado
---
La versión 1.9 de Okular ha sido publicada. Esta versión introduce el uso de cb7 para los archivos de cómics y mejoras en el uso de JavaScript en archivos PDF entre otras correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 es una actualización recomendada para todos los usuarios de Okular.
