---
date: 2008-04-02
title: Okular 0.6.3 publicado
---
La tercera versión de mantenimiento de la serie KDE 4.0 incluye Okular 0.6.3, que contiene unas cuantas correcciones de errores (como, por ejemplo, un modo mejor de obtener la posición del texto en un documento PDF) y algunas mejoras en el sistema de anotaciones y en la tabla de contenido. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
