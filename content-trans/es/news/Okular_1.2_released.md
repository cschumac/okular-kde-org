---
date: 2017-08-17
title: Okular 1.2 publicado
---
La versión 1.2 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 17.08. Esta versión contiene correcciones de errores y mejoras menores. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 es una actualización recomendada para todos los usuarios de Okular.
