---
date: 2009-12-01
title: Okular 0.9.4 publicado
---
El cuarto lanzamiento de la serie KDE 4.3 incluye Okular 0.9.4. Incluye algunos arreglos pequeños a la interfaz. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
