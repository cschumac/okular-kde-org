---
date: 2008-04-02
title: Okular 0.6.3 released
---
The third maintenance release of the KDE 4.0 series includes Okular 0.6.3. It includes few bug fixes, i.e. a better way to get the text position in a PDF document, and a couple of fixes for the annotation system, and the table of contents. You can read all the fixed issues at <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>